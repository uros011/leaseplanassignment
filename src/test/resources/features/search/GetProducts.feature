Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  @TEST01
  Scenario Outline: Searching for available products in product store with data
    When user call api service to retrieve <products> products
    Then user should receive status code 200
    And  user should see results displayed for <products> products
    Examples:
      | products |
      | apple    |
      | pasta    |
      | cola     |

  @TEST02
  Scenario: Searching for available products in product store without data
    When user call api service to retrieve orange products
    Then user should receive status code 200
    And  user should not see results displayed for orange product


  @TEST03
  Scenario: Searching not available product
    When user call api service to retrieve office products
    Then user should receive status code 404
    And  user should get appropriate product not found message