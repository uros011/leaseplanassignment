package starter.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;
import starter.apiservice.ProductsAPI;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

public class SearchStepDefinitions {

    @Steps
    public ProductsAPI productsAPI;

    @When("user call api service to retrieve {word} products")
    public void userCallApiServiceToProducts(String product) {
        productsAPI.get(product);
    }

    @Then("user should see results displayed for {word} products")
    public void userSeeTheResultsDisplayedFor(String product) {
        restAssuredThat(response -> response.body("any { it.containsKey('title') && it.title.contains('" + product + "')}", Matchers.is(true)));
    }

    @Then("user should not see results displayed for orange product")
    public void userNotSeeTheResultsDisplayedFor() {
        restAssuredThat(response -> response.body("isEmpty()", Matchers.is(true)));
    }

    @Then("user should receive status code {int}")
    public void userSeeStatusCodeInResponse(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Then("user should get appropriate product not found message")
    public void userShouldGetAppropriateMessage() {
        restAssuredThat(response -> response.body("detail.message", Matchers.containsString("Not found")));
        restAssuredThat(response -> response.body("detail.error", Matchers.is(true)));
    }

}
